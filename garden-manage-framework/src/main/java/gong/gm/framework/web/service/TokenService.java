package gong.gm.framework.web.service;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import gong.gm.common.constant.Constant;
import gong.gm.common.util.UUIDUtile;
import gong.gm.mapper.SysUserMapper;
import gong.gm.pojo.dto.SysUser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import gong.gm.common.redis.RedisCache;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author voloiono
 * @date 2024/7/8 11:41
 */
@Component
public class TokenService {
    @Autowired
    private RedisCache redisCache;

    @Autowired
    private SysUserMapper sysUserMapper;

    public void createToken(SysUser sysUser) {
        String token = UUIDUtile.fastUUID();
        sysUser.setToken(token);
        updateUserLoginInfo(sysUser);
        refreshUserExpireTime(sysUser);

        HashMap<String, Object> claims = new HashMap<>();
        claims.put(Constant.LOGIN_USER_KEY,token);
    }

    private void refreshUserExpireTime(SysUser sysUser) {

    }

    private void updateUserLoginInfo(SysUser sysUser) {
        sysUserMapper.update(new SysUser(), new LambdaUpdateWrapper<SysUser>()
                .set(SysUser::getToken, sysUser.getToken())
                .set(SysUser::getLoginTime, new Date())
                .set(SysUser::getLastLogin, new Date())
                .eq(SysUser::getUsername, sysUser.getUsername()));
    }

    //private String creatToken(Map claims){
//        return Jwts.builder()
//                .setClaims(claims)
//                .signWith(SignatureAlgorithm.HS256).compact();
    //}
}

package gong.gm.common.util;

import gong.gm.common.util.uuid.UUID;

/**
 * @author voloiono
 * @date 2024/7/8 14:53
 */
public class UUIDUtile {
    private UUIDUtile() {
    }

    public static String fastUUID(){
        return UUID.fastUUID().toString();
    }

    public static String simpleUUID(){
        return UUID.randomUUID().toString();
    }
}

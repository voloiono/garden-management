package gong.gm.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author:gong
 * @Date:2024/6/25 00:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WeatherInfoDto {
    private boolean isForecast;
    private String key;
}

package gong.gm.pojo.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author voloiono
 * @date 2024/6/25 11:49
 */
@Getter
@Setter
@TableName("sys_user")
public class SysUser {
    private Integer id;
    private String username;
    private String password;
    private String token;
    @TableField("`key`")
    private String key;
    private Integer status;
    private Integer userRole;
    private Date loginTime;
    private Date expireTime;
    private Date lastLogin;
}

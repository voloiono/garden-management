package gong.gm.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author voloiono
 * @date 2024/6/21 16:40
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WeatherDto {
    private String province;
    private String city;
    private String adcode;
    private String weather;
    private String temperature;
    private String windDirection;
    private String windPower;
    private String humidity;
    private String reportTime;
    private String temperatureFloat;
    private String humidityFloat;
    private String obsTime;
    private String temp;
    private String feelsLike;
    private String icon;
    private String text;
    private String wind360;
    private String windDir;
    private String windScale;
    private String windSpeed;
    private String precip;
    private String pressure;
    private String vis;
    private String cloud;
    private String dew;
    private String date;
    private String week;
    private String dayweather;
    private String nightweather;
    private String daytemp;
    private String nighttemp;
    private String daywind;
    private String nightwind;
    private String daypower;
    private String nightpower;
    private String daytemp_float;
    private String nighttemp_float;
}

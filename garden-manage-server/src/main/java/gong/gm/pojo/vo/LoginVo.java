package gong.gm.pojo.vo;

import lombok.Data;

/**
 * @author voloiono
 * @date 2024/7/2 16:20
 */
@Data
public class LoginVo {
    private String username;
    private String password;
}

package gong.gm.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author voloiono
 * @date 2024/7/4 11:01
 */
public class EncoderUtil {
    private EncoderUtil() {
    }

    private static final BCryptPasswordEncoder ENCODER = new BCryptPasswordEncoder();

    public static String encode(CharSequence rowPassword){
        return ENCODER.encode(rowPassword);
    }

    public static boolean match(CharSequence rawPassword,String encodePassword){
        return ENCODER.matches(rawPassword, encodePassword);
    }

}

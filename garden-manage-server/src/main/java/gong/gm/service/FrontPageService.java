package gong.gm.service;

import gong.gm.pojo.dto.WeatherDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author voloiono
 * @date 2024/6/21 16:39
 */
public interface FrontPageService {
  public List<WeatherDto> getLocalWeather(boolean isForecast,String key);
}

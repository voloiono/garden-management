package gong.gm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import gong.gm.pojo.dto.SysUser;
import gong.gm.pojo.vo.LoginVo;

/**
 * @author voloiono
 * @date 2024/6/25 11:50
 */
public interface SysUserService extends IService<SysUser> {
    SysUser queryUserByName(String name);

    String login(LoginVo loginVo);
}

package gong.gm.service;

import gong.gm.pojo.dto.SysUser;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author voloiono
 * @date 2024/7/8 10:33
 */
public interface UserManageService {
    List<SysUser> getUserInfoList();
}

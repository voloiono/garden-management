package gong.gm.service.impl;

import gong.gm.auth.entity.CustomUserDetail;
import gong.gm.mapper.SysUserMapper;
import gong.gm.pojo.dto.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

/**
 * @author voloiono
 * @date 2024/7/2 16:34
 */
@Service
public class UserDetaiServicelImpl implements UserDetailsService {
    @Autowired
    private SysUserMapper sysUserMapper;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser sysUser = sysUserMapper.GetUserByUserName(username);

        return new CustomUserDetail(sysUser, Collections.emptyList());
    }
}

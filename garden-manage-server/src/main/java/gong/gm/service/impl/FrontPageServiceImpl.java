package gong.gm.service.impl;

import cn.hutool.http.HttpGlobalConfig;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import gong.gm.pojo.dto.WeatherDto;
import gong.gm.service.FrontPageService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author voloiono
 * @date 2024/6/21 16:44
 */
@Service
public class FrontPageServiceImpl implements FrontPageService {
    private static final String WEATHER_URL_FORECAST = "https://restapi.amap.com/v3/weather/weatherInfo?parameters";

    private static final String WEATHER_URL_NOW = "https://devapi.qweather.com/v7/weather/now";

    @Cacheable(cacheNames = "weather_cache", key = "#key")
    @Override
    public List<WeatherDto> getLocalWeather(boolean isForecast, String key) {
        Map parms = new HashMap<>();

        String result = "";
        if (isForecast) {
            parms.put("key", "af025c257a041b70b5133f4c06f80dcb");
            parms.put("city", "510403");
            parms.put("extensions", "all");
            result = getResultByUrl(WEATHER_URL_FORECAST, parms);
        } else {
            parms.put("key", "d43698ae3dc74c9eb0158e2eb5c8fea3");
            parms.put("location", "101270206");
            result = getResultByUrl(WEATHER_URL_NOW, parms);
        }

        Map weatherPrimaryBean = JSONUtil.toBean(result, Map.class);
        ArrayList<WeatherDto> resultList = new ArrayList<>();

        if (isForecast) {
            JSONArray forecastList = JSONUtil.parseArray(weatherPrimaryBean.get("forecasts").toString());
            Object forecasts = forecastList.get(0);
            Map forecastBean = JSONUtil.toBean(forecasts.toString(), Map.class);
            JSONArray casts = JSONUtil.parseArray(forecastBean.get("casts"));
            for (Object cast : casts) {
                WeatherDto weatherDto = new WeatherDto();
                Map castMap = JSONUtil.toBean(cast.toString(), Map.class);
                weatherDto.setDate(castMap.get("date").toString());
                weatherDto.setWeek(castMap.get("week").toString());
                weatherDto.setDayweather(castMap.get("dayweather").toString());
                weatherDto.setNightweather(castMap.get("nightweather").toString());
                weatherDto.setDaytemp(castMap.get("daytemp").toString());
                weatherDto.setNighttemp(castMap.get("nighttemp").toString());
                weatherDto.setDaywind(castMap.get("daywind").toString());
                weatherDto.setNightwind(castMap.get("nightwind").toString());
                weatherDto.setDaypower(castMap.get("daypower").toString());
                weatherDto.setNightpower(castMap.get("nightpower").toString());

                resultList.add(weatherDto);
            }
        } else {
            String now = weatherPrimaryBean.get("now").toString();
            Map weatherBean = JSONUtil.toBean(now, Map.class);

            WeatherDto weatherDto = new WeatherDto();
            weatherDto.setObsTime(weatherBean.get("obsTime").toString());
            weatherDto.setTemp(weatherBean.get("temp").toString());
            weatherDto.setFeelsLike(weatherBean.get("feelsLike").toString());
            weatherDto.setIcon(weatherBean.get("icon").toString());
            weatherDto.setText(weatherBean.get("text").toString());
            weatherDto.setWind360(weatherBean.get("wind360").toString());
            weatherDto.setWindDir(weatherBean.get("windDir").toString());
            weatherDto.setWindScale(weatherBean.get("windScale").toString());
            weatherDto.setWindSpeed(weatherBean.get("windSpeed").toString());
            weatherDto.setHumidity(weatherBean.get("humidity").toString());
            weatherDto.setPrecip(weatherBean.get("precip").toString());
            weatherDto.setPressure(weatherBean.get("pressure").toString());
            weatherDto.setVis(weatherBean.get("vis").toString());
            weatherDto.setCloud(weatherBean.get("cloud").toString());
            weatherDto.setDew(weatherBean.get("dew").toString());

            resultList.add(weatherDto);
        }
        return resultList;
    }


    private String getResultByUrl(String url, Map parms) {
        String result = HttpRequest.get(url)
                .form(parms)
                .timeout(HttpGlobalConfig.getTimeout())
                .execute()
                .body();
        return result;
    }
}

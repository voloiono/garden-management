package gong.gm.base;

import gong.gm.enums.BaseResponseCode;
import lombok.Data;

import java.io.Serializable;

/**
 * @author voloiono
 * @date 2024/6/21 15:50
 */
@Data
public class R<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final int SUCCESS_CODE = BaseResponseCode.SUCCESS.getCode();

    public static final String SUCCESS_MSG = BaseResponseCode.SUCCESS.getMsg();

    private Integer code;
    private String message;
    private T data;

    public R() {
        this.code = SUCCESS_CODE;
        this.message = SUCCESS_MSG;
    }

    public R(String message) {
        this.code = SUCCESS_CODE;
        this.message = message;
    }

    public R(T data) {
        this.code = SUCCESS_CODE;
        this.message = SUCCESS_MSG;
        this.data = data;
    }

    public static <T> R<T> success(T data){
        R<T> r = new R<>();
        r.setCode(SUCCESS_CODE);
        r.setMessage(SUCCESS_MSG);
        r.setData(data);
        return r;
    }
}

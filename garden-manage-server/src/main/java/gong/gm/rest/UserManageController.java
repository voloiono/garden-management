package gong.gm.rest;

import gong.gm.base.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author voloiono
 * @date 2024/7/8 10:31
 */
@RestController
@RequestMapping("/system/manage")
public class UserManageController {
    @GetMapping("/list")
    public R getUserList(){
        return null;
    }
}

package gong.gm.rest;

import gong.gm.base.R;
import gong.gm.pojo.dto.WeatherDto;
import gong.gm.pojo.dto.WeatherInfoDto;
import gong.gm.service.FrontPageService;
import lombok.AllArgsConstructor;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.*;

/**
 * @author voloiono
 * @date 2024/5/22 15:44
 */
@RestController
@RequestMapping("/front")
@AllArgsConstructor
public class FrontPageController {
    private final FrontPageService frontPageService;

    @GetMapping("/weather")
    public R localWeather(@RequestBody WeatherInfoDto weatherInfoDto) {
        return R.success(frontPageService.getLocalWeather(weatherInfoDto.isForecast(), weatherInfoDto.getKey()));
    }
}

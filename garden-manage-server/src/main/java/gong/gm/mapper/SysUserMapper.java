package gong.gm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import gong.gm.pojo.dto.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author voloiono
 * @date 2024/6/25 12:00
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
    SysUser GetUserByUserName(@Param("username")String username);
}

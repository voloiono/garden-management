package gong.gm.enums;

/**
 * @author voloiono
 * @date 2024/6/21 16:05
 */
public enum BaseResponseCode {
    SUCCESS(200,"success")
    ;
    private final int code;
    private final String msg;

    BaseResponseCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}

package gong.gm.auth.entity;

import gong.gm.pojo.dto.SysUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * @author voloiono
 * @date 2024/6/25 11:20
 */
public class CustomUserDetail extends User {
    private SysUser sysUser;
    public CustomUserDetail(SysUser sysUser, Collection<? extends GrantedAuthority> authorities) {
        super(sysUser.getUsername(), sysUser.getPassword(), authorities);
        this.sysUser = sysUser;
    }

    public SysUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
}

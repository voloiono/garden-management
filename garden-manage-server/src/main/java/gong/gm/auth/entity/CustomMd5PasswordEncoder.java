package gong.gm.auth.entity;

import gong.gm.util.EncoderUtil;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.DigestUtils;

import java.util.Arrays;

/**
 * @author voloiono
 * @date 2024/6/25 11:12
 */
public class CustomMd5PasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence rawPassword) {
        System.out.println(EncoderUtil.encode(rawPassword));
        return EncoderUtil.encode(rawPassword);
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return EncoderUtil.match(rawPassword, encodedPassword);
    }

    public static void main(String[] args) {
        CustomMd5PasswordEncoder customMd5PasswordEncoder = new CustomMd5PasswordEncoder();
        System.out.println("111111");
        System.out.println(customMd5PasswordEncoder.encode("1234567"));
    }
}

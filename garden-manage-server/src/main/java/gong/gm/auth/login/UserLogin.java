package gong.gm.auth.login;

import gong.gm.auth.entity.CustomUserDetail;
import gong.gm.base.R;
import gong.gm.pojo.dto.SysUser;
import gong.gm.pojo.vo.LoginVo;
import gong.gm.service.SysUserService;
import gong.gm.service.impl.SysUserServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Author:gong
 * @Date:2024/6/26 00:05
 */
@RestController
@AllArgsConstructor
public class UserLogin {
    private final SysUserService sysUserService;
    @PostMapping("/login")
    public R doLogin(@RequestBody LoginVo loginUser){
        return R.success(sysUserService.login(loginUser));
    }
}

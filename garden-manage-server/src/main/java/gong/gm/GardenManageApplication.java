package gong.gm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author voloiono
 * @date 2024/5/22 15:38
 */
@SpringBootApplication
@EnableCaching
@MapperScan("gong.gm.mapper")
public class GardenManageApplication {
    public static void main(String[] args) {
        SpringApplication.run(GardenManageApplication.class,args);
    }
}
